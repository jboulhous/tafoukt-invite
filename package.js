Package.describe({
	summay : 'An invitation system for Meteor Accounts'
});

Package.on_use(function(api){
	//api.use('accounts-base',['client','server']);

	api.add_files('common.js',['client','server']);
	api.add_files('server.js','server');
	api.add_files('client.js','client');
});

Package.on_test(function(api){
	
	api.use(['invite','tinytest','test-in-browser']);

	// XXX I don't know yet why i have to add this
	// api.add_files('common.js',['client','server']);
	// api.add_files('server.js','server');
	// api.add_files('client.js','client');

	api.add_files('tests/client.js','client');
	api.add_files('tests/server.js','server');
});