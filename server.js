Invite.isInvited = function(email){
	Meteor._debug('Invite.isInvited');
	return !!Invite.Invitations.findOne({to:email});
}
// ...
Invite.check = function(userId,email){ return true; };
Invite.invite = function(userId,email){ return {}; };
Invite.recieve = function(userId,email){ return true; };

////////////////////////////////////////////////////////////////////////////////////
// Helpers 

function buildDefaultInvitation ( userId , email ) {
	Meteor._debug('buildDefaultInvitation');
	return {
		// default fields
		from : userId, 
		to : email,
		status : 0,

		// this field is let to the user ( a la 'profile' field of Accounts packages )
		subject : {}
	}
}
Invite.makeInvite = function ( user , email ){
	Meteor._debug('makeInvite');
	var invitation = buildDefaultInvitation(user,email);
  invitation.subject = Invite.invite(user,email);
  Meteor._debug('\ninvitation');
  invitation._id = Invite.Invitations.insert(invitation);
  return invitation;
};
Invite.makeRecieve = function ( email ){
	Meteor._debug('makeRecieve');
	// update invitation status
  Invite.Invitations.update({to:email},{$set:{status:1}},{multi:true}); // may be this email got invited from different users
  Meteor._debug('makeRecieve');
	return Invite.recieve(email);
};


Meteor.methods({
	"invite/invite" : function(email){
		check(email,Invite.ValidEmail);

		var user = this.userId,
				invitation;

		// check
		try{
			Invite.check(user,email);
		} catch ( error ){
			throw new Meteor.Error(403,error.message,'Invite check failed');
		}

		// invite
		try{
			invitation = Invite.makeInvite( user , email );
		} catch (error){
			throw new Meteor.Error(500,error.message,'Invite create invitation failed');
		}

		// return invitation
		return invitation;
	},
	"invite/recieve" : function( email ){
		// recieve
		return Invite.makeRecieve(email);
	}
});



