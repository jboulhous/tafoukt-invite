/**
 *
 */
Invite.invite = function( email , callback ){
	function cb(e,r){
		if(e){
			Meteor._debug ( 'Meteor.call("invite/invite") error' , e );
			callback ( e );
		} else {
			Meteor._debug ( 'Meteor.call("invite/invite") success' , r );
			callback ( r );
		}
	};

	return Meteor.call( 'invite/invite' , email , cb );
}