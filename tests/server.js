Tinytest.add('Invite - Namespace',function(test){
	// trivial should be tested too
	test.isTrue(Invite);
});

Tinytest.add('Invite - Collection',function(test){
	// trivial should be tested too
	test.isTrue(Invite.Invitations instanceof Meteor.Collection);
});


Tinytest.add('Invite - Invite.isInvited',function(test){
	var 
		isInvited = 'isinvited@meteorinvite.com',
		isNotInvited = 'isNotinvited@meteorinvite.com';

	Meteor.call('invite',isInvited);
	test.isTrue(Invite.isInvited(isInvited));
	test.isTrue(! Invite.isInvited(isNotInvited));
});

