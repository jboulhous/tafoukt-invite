if (typeof Invite === 'undefined'){
	Invite = {};
}

Invite.Invitations = new Meteor.Collection('invitations');

Invite.Invitations.allow({
	insert : function(userId,doc){ return false ;},
	update : function(userId,doc,fields){ return false ;},
	remove : function(userId,doc){ return false ;},
});

Invite.ValidEmail = Match.Where(function(val){
	// it must be a string (may be this is useless, as we use the regex)
	check(val,String);
	var emailRegex = /^[a-zA-Z0-9._]+[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-zA-Z]{2,4}$/;
	return emailRegex.test(val);
});

Invite.DuplicatedInvitation = Match.Where(function(options){
  // 
});

Invite.DeniedInvitation = Match.Where(function(options){
  // 
})

Invite.InvalideInvitation = Match.Where(function(options){
  // duplicated invitation
  // invitation to itsefl
  return true;
});